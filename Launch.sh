#!/bin/bash
./led_driver --led-rows=32 --led-cols=64 --led-gpio-mapping="adafruit-hat" --led-chain=2 --led-parallel=1 -f ./rpi-rgb-led-matrix/fonts/10x20.bdf --led-pixel-mapper="Rotate:180"
