// -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
// Example of a clock. This is very similar to the text-example,
// except that it shows the time :)
//
// This code is public domain
// (but note, that the led-matrix library this depends on is GPL v2)

#include "led-matrix.h"
#include "graphics.h"
#include "common/common.h"
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <string>

using namespace rgb_matrix;

volatile bool interrupt_received = false;
static void InterruptHandler(int signo);


static int usage(const char *progname);
static bool parseColor(Color *c, const char *str);

int main(int argc, char *argv[])
{
  settings mysettings;
  socket_t controller;
  controller.setup_client(mysettings.get_led_socket().c_str());    
    
  char * currentRoute = nullptr;

  RGBMatrix::Options matrix_options;
  rgb_matrix::RuntimeOptions runtime_opt;
  if (!rgb_matrix::ParseOptionsFromFlags(&argc, &argv,
                                         &matrix_options, &runtime_opt))
  {
    return usage(argv[0]);
  }

  Color color(255, 0, 0);
  Color bg_color(0, 0, 0);
  Color outline_color(0,0,0);
  bool with_outline = false;

  const char * bdf_font_file = NULL;
  int x_orig = 0;
  int y_orig = 0;
  int brightness = 100;
  int letter_spacing = 0;

  int opt;
  while ((opt = getopt(argc, argv, "x:y:f:C:B:O:b:S:")) != -1) {
    switch (opt) {
    case 'b': brightness = atoi(optarg); break;
    case 'x': x_orig = atoi(optarg); break;
    case 'y': y_orig = atoi(optarg); break;
    case 'f': bdf_font_file = strdup(optarg); break;
    case 'S': letter_spacing = atoi(optarg); break;
    case 'C':
      if (!parseColor(&color, optarg))
      {
        fprintf(stderr, "Invalid color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'B':
      if (!parseColor(&bg_color, optarg))
      {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'O':
      if (!parseColor(&outline_color, optarg))
      {
        fprintf(stderr, "Invalid outline color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      with_outline = true;
      break;
    default:
      return usage(argv[0]);
    }
  }

  if (bdf_font_file == NULL)
  {
    fprintf(stderr, "Need to specify BDF font-file with -f\n");
    return usage(argv[0]);
  }

  /*
   * Load font. This needs to be a filename with a bdf bitmap font.
   */
  rgb_matrix::Font font;
  if (!font.LoadFont(bdf_font_file))
  {
    fprintf(stderr, "Couldn't load font '%s'\n", bdf_font_file);
    return 1;
  }
  rgb_matrix::Font *outline_font = NULL;
  if (with_outline)
  {
      outline_font = font.CreateOutlineFont();
  }

  if (brightness < 1 || brightness > 100)
  {
    fprintf(stderr, "Brightness is outside usable range.\n");
    return 1;
  }

  RGBMatrix *matrix = rgb_matrix::CreateMatrixFromOptions(matrix_options,
                                                          runtime_opt);
  if (matrix == NULL)
    return 1;

  matrix->SetBrightness(brightness);

  const int x = x_orig;
  int y = y_orig;

  FrameCanvas *offscreen = matrix->CreateFrameCanvas();

  char text_buffer[256];
  strcpy(text_buffer, "Hello from LED");
  signal(SIGTERM, InterruptHandler);
  signal(SIGINT, InterruptHandler);
  Stop * TempStop = nullptr;
  bool stopReq = false;
  bool DisplayStop = true;
  char * TempStopStr = nullptr;
  char * TempReqStr = nullptr;
  char RouteStr[20];
  rgb_matrix::DrawText(offscreen, font, x, y + font.baseline(),
                       color, NULL, text_buffer,
                       letter_spacing);
  offscreen = matrix->SwapOnVSync(offscreen);

  while (!interrupt_received)
  {
      /*Get String from Socket*/
      if(controller.data_avail())
      {
        delete TempStop;
        TempStop = nullptr;
        char * TempStr = (char *)controller.receive_msg(500);
        printf("Received: %s\n", TempStr);
        currentRoute = strtok(TempStr, ":");
        TempStopStr = strtok(nullptr, ":");
        printf("StopSr: %s\n", TempStopStr);
        TempReqStr = strtok(nullptr, ":");
        printf("ReqStr: %s\n", TempReqStr);
        TempStop = new Stop(TempStopStr);
        stopReq = (strcmp("1", TempReqStr) == 0);
        /*Copy to Buffer*/
        strcpy(RouteStr, "Route ");
        strcat(RouteStr, currentRoute);
        if(stopReq)
        {
          DisplayStop = false;
        }
        else
        {
          DisplayStop = true;
        }

      }
      if(TempStop != nullptr)
      {
      if(DisplayStop)
      {
        strcpy(text_buffer, TempStop->Name.c_str());
        DisplayStop = false;
      }
      else if(!DisplayStop && stopReq)
      {
        strcpy(text_buffer, "Stop Request");
        DisplayStop = true;
      }
      else
      {
        strcpy(text_buffer, RouteStr);
        DisplayStop = true;
      }
      /*Display to LED Panel*/
      offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);
      if (outline_font)
      {
        rgb_matrix::DrawText(offscreen, *outline_font,
                             x - 1, y + font.baseline(),
                             outline_color, NULL, text_buffer,
                               letter_spacing - 2);
      }
      rgb_matrix::DrawText(offscreen, font, x, y + font.baseline(),
                         color, NULL, text_buffer,
                         letter_spacing);
      // Atomic swap with double buffer
      offscreen = matrix->SwapOnVSync(offscreen);
      sleep(5);
    }
  }
  // Finished. Shut down the RGB matrix.
  matrix->Clear();
  delete matrix;
  write(STDOUT_FILENO, "\n", 1);  // Create a fresh new line after ^C on screen
  return 0;
}

static void InterruptHandler(int signo)
{
  interrupt_received = true;
}

static bool parseColor(Color *c, const char *str)
{
  return sscanf(str, "%hhu,%hhu,%hhu", &c->r, &c->g, &c->b) == 3;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options]\n", progname);
  fprintf(stderr, "Reads text from stdin and displays it. "
          "Empty string: clear screen\n");
  fprintf(stderr, "Options:\n");
  rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-f <font-file>    : Use given font.\n"
          "\t-b <brightness>   : Sets brightness percent. Default: 100.\n"
          "\t-x <x-origin>     : X-Origin of displaying text (Default: 0)\n"
          "\t-y <y-origin>     : Y-Origin of displaying text (Default: 0)\n"
          "\t-S <spacing>      : Spacing pixels between letters (Default: 0)\n"
          "\t-C <r,g,b>        : Color. Default 255,255,0\n"
          "\t-B <r,g,b>        : Background-Color. Default 0,0,0\n"
          "\t-O <r,g,b>        : Outline-Color, e.g. to increase contrast.\n"
          );

  return 1;
}
