RGB_LIB_DISTRIBUTION=rpi-rgb-led-matrix
RGB_INCDIR=$(RGB_LIB_DISTRIBUTION)/include
RGB_LIBDIR=$(RGB_LIB_DISTRIBUTION)/lib
RGB_LIBRARY_NAME=rgbmatrix
RGB_LIBRARY=$(RGB_LIBDIR)/lib$(RGB_LIBRARY_NAME).a
LDFLAGS+=-L$(RGB_LIBDIR) -l$(RGB_LIBRARY_NAME) -lrt -lm -lpthread

include common/common.mk

all: led_driver
CC = g++
WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
SOURCE_FILES += led_driver.cpp
//OPTIMIZE = -O2

$(RGB_LIBRARY): FORCE
	$(MAKE) -C $(RGB_LIBDIR)

led_driver: Makefile led_driver.cpp $(RGB_LIBRARY)
	$(CC) -I$(RGB_INCDIR) $(WARNINGS) $(DEBUG) $(OPTIMIZE) $(SOURCE_FILES) -o $@ $(LDFLAGS)

clean:
	rm -f led_driver

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	sudo ./led_driver

FORCE:
.PHONY: FORCE
